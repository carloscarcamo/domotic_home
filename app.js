
/**
 * Module dependencies.
 */

var express = require('express');
//var routes = require('./routes');
//nueva linea
var user = require('./routes/user');
var http = require('http');
var path = require('path');
/*incluyendo el archivo maproutecontroller.js
 * que se encarga de erutar los archivos en la
 * carpeta /controllers
 */
var map = require('./maproutecontroller');
var app = express();


// all environments
//Configuracion
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser());//necesario para utilizar sesiones
app.use(express.bodyParser());
app.use(express.session({secret : "jiojfkvifosasdetdcroerkb",cookie: {maxAge: 600000}}));//necesario para utilizar sesiones
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//Routes
/*app.get('/', routes.index);
app.get('/index', routes.index);*/
//nueva linea
require('./routes')(app);

//Enrutando llamadas al los archivos en la carpeta /controllers
map.mapRoute(app, 'serial_connect');
map.mapRoute(app, 'getJsonData');


//iniciando el servers
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
