/**
 * @author winbugs
 */

function ie8SafePreventEvent(event){
	(event.preventDefault)? event.preventDefault():event.returnValue = false;
}

$(document).ready(function(){	
	var interval; 
	 
	$('.carousel').carousel('pause');
	
	//conectarse al puerto
	requests.conectarse();		
	interval = setInterval(function(){
		requests.leerDatos();
	},5000);
	
	//cerrar session
	$("#logOut").click(function(e){
		ie8SafePreventEvent(e);		
		requests.desconectarse();
		clearInterval(interval);
		setTimeout(function(){
			window.location.href = "http://localhost:3000/" + "logout";	
		}, 1000);		
	});
	
	//encender/apagar 
	$(".led").each(function(){
		$(this).click(function(){
			var state = ($(this).attr('state') == '0')? true:false;
			requests.escribirDatos($(this).attr('id'), state);			
		});
	});
	
});