/**
 * @author winbugs
 */
var _url = "http://localhost:3000/";

function ie8SafePreventEvent(event){
	(event.preventDefault)? event.preventDefault():event.returnValue = false;
}

$(document).ready(function(){
	//enviar el formulario
	$("#logForm").on("submit", function(e){
		ie8SafePreventEvent(e);
		//validar textos
		var form = $("#logModal").validar({					
			text:{required:true, minLength:6, maxLength:20},						
			mail:{required:true, minLength:10, maxLength:50}			
		},{
			text:"",
			mail:""
		});
		
		
		if(form){
			login("login", {mail:$("#mail").val(), password:$("#password").val()});
		}else{
			shake($("#logModal"));
		}
	});
});

//iniciar session
function login(url, data){
	$.post(url, data, function(data){		
		if(data == "Error"){
			//alert("Correo o contraseña incorrectos!!!");
			mostrarError($("#logModal"));
			shake($("#logModal"));
		}else{
			limpiarError($("#logModal"));
			window.location.href = _url + "home";
		}
	});
}

function shake(element){
	element.animate({
		left:'+=50'
	},75, function(){
		$(this).animate({					
			left:'-=100'
		},75, function(){					
			$(this).animate({						
				left:'+=50'
			},75);
		});
	});
}

function mostrarError(elemento){
	var _msgStyles = {
		"float": "right",
		"font-size": "12px",
		"color":"#FF190D",
		"position":"relative",					
		"z-index":10
	};
	
	var _errorStyles = {
		"background":"#FFACA4",
		"border":"1px solid #FF190D"
	};
	
	elemento.find("input").each(function(index){		
		$(this).css(_errorStyles);		
	});
	
	$("span#"+elemento.attr('id')).remove();
	$("<span id='"+elemento.attr('id')+"'>Email o password incorrectos!!!</span>")
	.css(_msgStyles).insertAfter("#btnLog");
}

function limpiarError(elemento){
	elemento.find("input").each(function(index){
		$(this).removeAttr("style");		
	});
	
	$("span#"+elemento.attr('id')).remove();
}
