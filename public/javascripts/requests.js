/**
 * @author winbugs
 */

//Peticiones
var requests = (function(){		
	//Funcion para actualizar imagenes y botones 
	var actualizarImgs = function(datos){
		var n = datos.split("");	
		console.log(n[0]);				
		$('#I1').attr('src','/images/habitacion1/'+n[0]+n[1]+'.jpg');
		$('#I2').attr('src','/images/habitacion2/'+n[2]+n[3]+'.jpg');
		$('#I3').attr('src','/images/habitacion3/'+n[4]+n[5]+'.jpg');
		$('#I4').attr('src','/images/sala/'+n[6]+'.jpg');
		$('#I5').attr('src','/images/jardin/'+n[7]+n[8]+'.jpg');
		$('#I6').attr('src','/images/patio/'+n[9]+'.jpg');
		$('#I7').attr('src','/images/banhio/'+n[10]+'.jpg');
		$('#I8').attr('src','/images/piscina/'+n[11]+'.jpg');
		$('#I9').attr('src','/images/garage/'+n[12]+'.jpg');
		$(".led").each(function(index){
			$(this).attr('state', n[index]);
			if(n[index] == "0"){					
				if($(this).hasClass('btn-warning')){
					$(this).removeClass('btn-warning').addClass('btn-default');						
				}
				btnTextUpdateOn($(this));					
			}else{					
				if($(this).hasClass('btn-default')){
					$(this).removeClass('btn-default').addClass('btn-warning');
				}
				btnTextUpdateOff($(this));					
			}	
		});	
	};
	
	//Actualizar texto de botones apagados
	var btnTextUpdateOn = function(btn){
			var span = btn.find("span.glyphicon-star-empty");
		var txt = span.text().trim();
		if(txt == "Apagar izquierda"){
			span.text("Encender izquierda");
		}
			
		if(txt == "Apagar derecha"){
			span.text("Encender derecha");
		}
						
		if(txt == "Apagar"){
			span.text("Encender");
		}
			
		if(txt == "Cerrar"){
			span.text("Abrir");		
		}
	};
	
	//Actualizar texto de botones encendidos
	var btnTextUpdateOff = function(btn){
		var span = btn.find("span.glyphicon-star-empty");
		var txt = span.text().trim();
		if(txt == "Encender izquierda"){
			span.text("Apagar izquierda");
		}
			
		if(txt == "Encender derecha"){
			span.text("Apagar derecha");
		}
						
		if(txt == "Encender"){
			span.text("Apagar");
		}
			
		if(txt == "Abrir"){
			span.text("Cerrar");
		}
	};
	
	//metodos publicos
	return {
		
		//funcion para abrir la conexion con el puerto
		conectarse: function(){			
			$.post('serial_connect', {connect: true}, function(data){
				if(typeof data.error == 'undefined'){
					alert(data.msg);
					console.log("Lectura: " + data.resultado);
					actualizarImgs(data.resultado);
					$('.carousel').carousel('next');
					$('.led').each(function(){
						$(this).removeAttr('disabled');
					});					
					$('#close').removeAttr('disabled');
					$('#leer').removeAttr('disabled');					
				}else{
					alert(data.error);
					
				}						
			}).fail(function(){
				alert('Problemas con la peticion');				
			});			
		},
		
		//funcion para terminar conexion con el puerto
		desconectarse: function(){
			$.post('serial_connect', {close: true}, function(data){
				alert(data.msg);
				$('#open').removeAttr('disabled');
				$('#leer').attr("disabled","disabled");
				$('.led').each(function(){
						$(this).attr("disabled","disabled");
				});
			}).fail(function(){
				alert('Problemas con la peticion');
			});
		},
		
		//funcion para leer datos
		leerDatos: function(){
			$.post("serial_connect", {read:true}, function(data){
				if(typeof data.resultado != undefined){
					console.log("Datos: " + data.resultado);
					actualizarImgs(data.resultado);					
				}
			}).fail(function(){
				alert('Problemas con la peticion');
			});
			
		}, 		
	
		//funcion para escribir 
		escribirDatos: function(dato, state){				
			datos = {
				write: true,
				on:state,
				led: dato
			};
			
			$.post('serial_connect', datos, function(data){			
				if(typeof data.resultado != undefined){
					console.log("Datos: " + data.resultado);	
					actualizarImgs(data.resultado);					
				}	
			}).fail(function(){
				alert('Problemas con la peticion');
			});				
			
		}
	};
}());