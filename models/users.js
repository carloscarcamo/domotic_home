/**
 * @author winbugs
 */
var fs = require('fs'),
sqlite3 = require('sqlite3').verbose(),
dataFile = "domotic_home",
UserModel = {};

UserModel.loginUser = function(uData, callback){	
	console.log(fs.existsSync("domotic_home"));
	var db = new sqlite3.Database(dataFile, function(err){
		if(err){
			console.log("Error al conectarse con la DB");
		}
	});
	//console.log("Params: " + uData.mail + ", " + uData.pass);
	var stmt = db.prepare('SELECT * FROM usuarios WHERE email = ? AND password = ?', uData.mail, uData.pass, function(err){
		if(err){
			console.log("Error en la query");
		}
	});
	
	stmt.get(function(err, row){
		if(err){
			console.log(err);
			throw err;			
		}else{
			if(row){
				//console.log(row);
				callback({msg:"Logueado", data:row});
			}else{
				console.log("Not Data from query: " + row);
				callback({msg:"Error", data:""});
			}
		}
		
	}); 		
			
};

module.exports = UserModel;