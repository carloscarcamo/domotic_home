/**
 * @author winbugs
 */

//Este codigo enruta las llamadas a los archivos en la carpeta controllers
exports.mapRoute = function(app, prefix) {
	prefix = '/' + prefix;
	var prefixObj = require('./controllers/' + prefix);
	
	// routing
	app.post(prefix, prefixObj[prefix.substr(1,prefix.length)]);
	
	
};