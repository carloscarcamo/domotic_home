
/*
 * GET home page.
 */

/*exports.index = function(req, res){
  	res.render('index', { title: 'Domotic Home' });
};*/

var UserModel = require("../models/users");

module.exports = function(app){
	app.get('/', function(req, res){
		if(req.session.mail){
			res.redirect('/home');
		}
		
		res.render('index', {title:"Domotic Home", usuario:""});
	});
	
	//cuando se envia el formulario de login
	app.post('/login', function(req, res){
		/*console.log("attempt to log");
		console.log("mail: " + req.body.mail + "\nPass: " + req.body.password);*/
		UserModel.loginUser({mail:req.body.mail, pass:req.body.password}, function(data){
			if(data){
				if(data.msg == "Error"){										
					res.send("Error", 200);
				}else{
					req.session.mail = req.body.mail;					
					res.send("Logueado", 200);
				}
			}else{				
				res.send("Error", 200);
			}
		});
	});
	
	app.get('/home', function(req, res){
		if(!req.session.mail){
			res.redirect("/");
		}else{
			res.render("home", {
				title: "Bienvenido a tu Casa Domotica",				
				usuario: req.session.mail.split("@")[0]
			});
		}	
	});
	
	app.get('/logout', function(req, res){
		req.session.destroy();
		res.redirect('/');
	});
};
