/**
 * @author winbugs
 */
var serialFunctions = require("./serialFunctions.js");
var serial;
exports.serial_connect = function(req, res){	
	var datos = new Object();
	//Conectar con el puerto
	if(req.body.connect){		
		console.log("************ Conectando a casa domotica ************");
		serial = serialFunctions();
		serial.connectToPort("COM9", req, res);		
	}
	
	//cerrando conexion
	if(req.body.close){
		console.log("************ Cerrando conexion a casa domotica ************");
		serial.closePort(req, res);		
	}	
	
	//Leyendo datos del pic
	if(req.body.read){
		console.log("************ Leyendo estados de dispositivos ************");
		serial.readPort(req, res);				
	}
	
	//escribiendo en el pic
	if(req.body.write){	
		console.log("************ Activando dispositivos ************");
		serial.writeToPort(req, res);		
	}	
};