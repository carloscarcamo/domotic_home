/**
 * @author winbugs
 */
var SerialPort = require("serialport").SerialPort;
var fs = require('fs');
var serialPort;

module.exports = function(){	
	var leds = {
		l0: {on:'01', off:'02'},
		l1: {on:'03', off:'04'},
		l2: {on:'05', off:'06'},
		l3: {on:'07', off:'08'},
		l4: {on:'09', off:'10'},
		l5: {on:'11', off:'12'},
		l6: {on:'13', off:'14'},
		l7: {on:'15', off:'16'},
		l8: {on:'17', off:'18'},
		l9: {on:'19', off:'20'},
		l10: {on:'21', off:'22'},
		l11: {on:'23', off:'24'},
		l12: {on:'25', off:'26'}
	};
	var datos = new Object();
	
	//funcion para leer datos directamente del pic
	var reading = function(req, res){		
		serialPort.write('ff', function(err, results){
			if(err){
				datos.error = err;
				console.log(err);
			}
			serialPort.on('data', function(data) {
				if(data != 0 && data != undefined && data != "f" && data.length > 13){
					//console.log("datos del pic: " + data);
					var bin = data.slice(-13);
					console.log("datos: " + bin);						
					//Guardando registros en archivo data.txt
					fs.writeFileSync('./data.txt', bin);						
					
					//esperar un tiempo para enviar los datos actualizados de la lectura
					setTimeout(function(){
						var contenido = new Buffer(fs.readFileSync('./data.txt'));	
						datos.resultado = contenido.toString();					
						/*res.json(datos);
						res.end();*/
						res.send(datos);
					}, 200);	
				}				
			});									
		});		
	};
	
	
	
	//covert hex to binary
	var hex2bin = function(hex, bits){
		var bin = parseInt(hex, 16).toString(2).split('').reverse();
	  	if(bin.length<8){
	    	var ceros = bits - bin.length;
	    	for(var i=0; i<ceros; i++){
	      		bin.push('0');
	    	}     
	  	}  
	  	
	  	return bin.join('');
	};
	
	//metodos publicos
	return {		
		//Funcion para conectarse con el puerto
		connectToPort: function(port, req, res){
			serialPort = new SerialPort(port, {
				baudrate: 9600
			}, false);
			
			//open event
			serialPort.open(function(){
				console.log("Puerto abierto: " + port);				
			});
			
			//este evento se ejecuta cuando se conecta con el puerto
			serialPort.on('open', function(data){			
				console.log('Conexion establecida!!!');
				datos.msg = "Conexion establecida!!!";
				
				//al conectarse se leera el pic
				reading(req, res);		
			});			
		},
		
		//Cerrar la conexion activa
		closePort: function(req, res){
			serialPort.close(function(){			
				datos.msg = "Se ha cerrado la conexión!!!";
				console.log('Se ha cerrado la conexión!!!');
				res.send(datos);
				/*res.json(datos);
				res.end();*/
			});			
		},
		
		//Leer datos del pic 
		readPort: function(req, res){
			reading(req, res);
		},
		
		//Escribir en el pic
		writeToPort: function(req, res){	
			console.log(req.body.on);		
			var led = (req.body.on == "true")?leds[req.body.led].on:leds[req.body.led].off;		
			serialPort.write(led, function(err, results) {
				if(err){ 
					console.log('Errores ' + err);
					console.log("Escribiendo en el pic: " + led);
					console.log('Bits escritos: ' + results);
				}
				console.log("Escribiendo en el pic: " + led);
				reading(req, res);
			});
			
						
		}
	};
};