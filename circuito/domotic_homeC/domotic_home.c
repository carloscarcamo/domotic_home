#include <18F4550.h>             //PIC 

#fuses  HSPLL, NOWDT, NOPROTECT, NOLVP, NODEBUG, USBDIV, PLL5, CPUDIV1, VREGEN //configuracion de fusibles

#use delay(clock=20000000)    //Frecuencia del cristal oscilador externo

#include <pic18_usb.h>
#include <usb_cdc.h>//Librer�a de control USB
#include <usb_desc_cdc.h> // Descriptores del dispositivo USB.
#include <stdio.h>
#include <stdlib.h>
#define USB_CON_SENSE_PIN PIN_B0 //Pin de detecci�n conexi�n-desconexi�n dispositivo USB

#byte PORTA = 0xf80
#byte PORTB = 0xf81
#byte PORTC = 0xf82
#byte PORTD = 0xf83
#byte PORTE = 0xf84
#byte TRISA = 0xf92
#byte TRISB = 0xf93
#byte TRISC = 0xf94
#byte TRISD = 0xf95
#byte TRISE = 0xf96

#use fast_io(a)
#use fast_io(b)
#use fast_io(c)
#use fast_io(d)
#use fast_io(e)

void lectura_Leds() {  
   char *pines[14];
   char salida[14];
   int i;
   pines[0] = PIN_B0;
   pines[1] = PIN_B1;
   pines[2] = PIN_B2;
   pines[3] = PIN_B3;
   pines[4] = PIN_B4;
   pines[5] = PIN_B5;
   pines[6] = PIN_B6;
   pines[7] = PIN_B7;
   pines[8] = PIN_E0;
   pines[9] = PIN_E1;
   pines[10] = PIN_E2;
   pines[11] = PIN_C2;
   pines[12] = PIN_A4;
  // pines[13] = NULL;   
   for(i=0; i<13; i++){
      if(input(pines[i])){
         salida[i] = "1";
      }else{
         salida[i] = "0";
      }
   }   
   salida[13] = NULL;
   
   printf(usb_cdc_putc, "%s", salida);   //lectura
}

void motor_right(){ 
   output_high(PIN_A0);;
   output_low(PIN_A1);      
}

void motor_left(){
   output_high(PIN_A1);
   output_low(PIN_A0);
}
void apagar_todo(){
   output_a(0);
   output_b(0);
   output_c(0);
   output_d(0);  
   output_e(0);
}
//Encender leds con xors
void ledSwitchOn(PIN_IN, PIN_OUT){
   if(!input(PIN_IN) && !input(PIN_OUT)){
      output_high(PIN_OUT);
   }else if(!input(PIN_IN) && input(PIN_OUT)){
      output_low(PIN_OUT);
   } 
}
//Apagar leds con xors
void ledSwitchOff(PIN_IN, PIN_OUT){
   if(input(PIN_IN) && input(PIN_OUT)){
      output_low(PIN_OUT);
   }else if(input(PIN_IN) && !input(PIN_OUT)){
      output_high(PIN_OUT);
   }
}

void encender_remoto(){
   BYTE value;
   value = gethex_usb();

   switch(value)
   {
      case 0x00:
         apagar_todo();
         break;
      case 0x01:         
         ledSwitchOn(PIN_B0, PIN_D0);
         break;
      case 0x02:         
         ledSwitchOff(PIN_B0, PIN_D0);
         break;
      case 0x03:         
         ledSwitchOn(PIN_B1, PIN_D1);
         break;
      case 0x04:                  
         ledSwitchOff(PIN_B1, PIN_D1);
         break;
      case 0x05:                  
         ledSwitchOn(PIN_B2, PIN_D2);
         break;
      case 0x06:                  
         ledSwitchOff(PIN_B2, PIN_D2);
         break;   
      case 0x07:                  
         ledSwitchOn(PIN_B3, PIN_D3);
         break;
      case 0x08:         
         ledSwitchOff(PIN_B3, PIN_D3);
         break;
      case 0x09:         
         ledSwitchOn(PIN_B4, PIN_D4);
         break;
      case 0x10:         
         ledSwitchOff(PIN_B4, PIN_D4);
         break;
      case 0x11:          
         ledSwitchOn(PIN_B5, PIN_D5);
         break;
      case 0x12:          
         ledSwitchOff(PIN_B5, PIN_D5);
         break;
      case 0x13:          
         ledSwitchOn(PIN_B6, PIN_D6);
         break;
      case 0x14:          
         ledSwitchOff(PIN_B6, PIN_D6);
         break;
      case 0x15:          
         ledSwitchOn(PIN_B7, PIN_D7);
         break;
      case 0x16:          
         ledSwitchOff(PIN_B7, PIN_D7);
         break;
      case 0x17:                   
         ledSwitchOn(PIN_E0, PIN_C0);
         break;
      case 0x18:                   
         ledSwitchOff(PIN_E0, PIN_C0);
         break;
      case 0x19:                   
         ledSwitchOn(PIN_E1, PIN_C1);
         break;
      case 0x20:                   
         ledSwitchOff(PIN_E1, PIN_C1);
         break;
      case 0x21: 
         output_high(PIN_C6);
         break;
      case 0x22: 
         output_low(PIN_C6);
         break;
      case 0x23: 
         output_high(PIN_C7);
         break;
      case 0x24: 
         output_low(PIN_C7);
         break;
      case 0x25: 
         motor_right();
         break;
      case 0x26: 
         motor_left();
         break;
      case 0xff: 
         lectura_Leds();
         break;   
      default: 
         apagar_todo();
         break;
   }
}

void main() {
   delay_ms(300);
   SET_TRIS_A(0x78);
   SET_TRIS_B(0xff);
   SET_TRIS_D(0x00);   
   SET_TRIS_C(0x04);
   SET_TRIS_E(0x0f); 
   output_a(0);
   output_b(0);
   output_c(0);
   output_d(0);  
   output_e(0);
   usb_cdc_init();
   usb_init(); //inicializamos el USB
   while (!usb_cdc_connected()) {}
   do{
      usb_task();
      if(usb_enumerated()){ //retorna verdadero si el dispositivo esta enumerado como COM
         //apagar motor al tocar un pulsador
         if((input(PIN_A4) && input(PIN_A0)) || (input(PIN_A5) && input(PIN_A1))){
            output_low(PIN_A0);
            output_low(PIN_A1);
         }
         if(usb_cdc_kbhit()){            
            
            encender_remoto();
         }
      }
   }while(true);
}
